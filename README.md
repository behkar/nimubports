
# Nim For UT


A template to support the Nim programming language for UBports.



<img src="https://gitlab.com/behkar/nimubports/-/raw/main/logo.png"  width="200" height="250">




<img src="https://gitlab.com/behkar/nimubports/-/raw/main/nim_ubport.png"  width="60" height="120">Note :**The project is in progress and will be merged into the main Clickable sub-branch when finished.**


## Roadmap & Features

- [x] x86_64 Support
- [ ] ARMv7/ARM64 support
- [ ] Nimble Support
- [x] The latest version of Nim compiler (v1.6.6)

## Installation

Required Tools: Only Clickable is required:
https://clickable-ut.dev/en/latest/install.html

Compile and install:
```bash
  git clone https://gitlab.com/behkar/nimubports.git
  cd nimubports/
  clickable desktop
```

Note :

**Nimble is currently not working.**
**No need to install nim compiler and dotherside library.**


## Authors

- [@behkar](https://www.gitlab.com/behkar)
- [@Jonatan Hatakeyama Zeidler](https://gitlab.com/jonnius)
